FROM node:14
COPY package.json ./
RUN npm install
RUN npm install -g nodemon
RUN mkdir -p work 
WORKDIR /work
COPY . .